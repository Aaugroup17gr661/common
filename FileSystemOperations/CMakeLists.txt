cmake_minimum_required(VERSION 2.8.3)
project(FileSystemOperations)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  roslib
)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES FileSystemOperations
  CATKIN_DEPENDS
  roscpp
  roslib
  DEPENDS Boost
)

include_directories(
  ${catkin_INCLUDE_DIRS}
  ${Boost_INCLUDE_DIRS}
  include
)

add_library(FileSystemOperations FileSystemOperations.cpp)

target_link_libraries(FileSystemOperations
                      ${catkin_LIBRARIES}
                      ${Boost_LIBRARIES}
                      )
                      
install(TARGETS FileSystemOperations
        ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
        RUNTIME DESTINATION ${CATKIN_GLOBAL_BIN_DESTINATION})
                      
                      
install(DIRECTORY include/
        DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
        FILES_MATCHING PATTERN "*.h")
